//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import Foundation

struct Phone {
    let label: String
    let phoneNumber: String
    let isMain: Bool
}

extension Phone: Equatable {}
