//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

final class ContactsListItemCell: UITableViewCell {

    private let thumbnail = AvatarView(style: .thumbnail)

    private let nameLabel = UILabel(style: .title3, lines: 1)
    private let phoneLabel = UILabel(style: .body, lines: 1)

    private lazy var mainContent: UIView = {
        UIStackView(.horizontal, spacing: 16, alignment: .center, views: [
            thumbnail,
            UIStackView(.vertical, spacing: 8, views: [nameLabel, phoneLabel])
        ])
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        thumbnail.cleanup()

        nameLabel.set(text: nil)

        phoneLabel.set(text: nil)
        phoneLabel.isHidden = true
    }

    func setModel(_ model: Contact) {
        // Thumbnail
        thumbnail.setAvatar(model.avatar)

        // Full name
        nameLabel.set(text: .attributed(accentedFullNameForModel(model: model)))

        // Phone
        phoneLabel.text = model.phones.mainOrNext?.phoneNumber
        phoneLabel.isHidden = phoneLabel.text?.isEmpty ?? true
    }
}

private extension ContactsListItemCell {
    func setup() {
        contentView.addSubview(mainContent)
        mainContent.snp.makeConstraints {
            $0.edges.equalToSuperview().inset(16)
            $0.height.greaterThanOrEqualTo(60)
        }
    }

    func accentedFullNameForModel(model: Contact) -> NSAttributedString {
        let normalAttr: Attributes = [.font: UIFont.regular17, .foregroundColor: UIColor.appForeground]
        let accentAttr: Attributes = [.font: UIFont.bold17, .foregroundColor: UIColor.appForeground]

        let nameComponents: [(text: String, attr: Attributes)] = {
            var array = [(text: model.givenName, attr: normalAttr),
                         (text: model.middleName, attr: normalAttr),
                         (text: model.familyName, attr: normalAttr)]
            switch model.accentName {
            case .given:
                array[0].attr = accentAttr
            case .middle:
                array[1].attr = accentAttr
            case .family:
                array[2].attr = accentAttr
            case .organization:
                array.append((text: model.organizationName, attr: accentAttr))
            case .none:
                break
            }
            return array
        }()

        let strings: [NSAttributedString] = nameComponents.compactMap {
            guard !$0.text.isEmpty else { return nil }
            return NSAttributedString(string: $0.text + " ", attributes: $0.attr)
        }

        let fullName = NSMutableAttributedString(string: "")
        strings.forEach { fullName.append($0) }
        return fullName
    }
}
