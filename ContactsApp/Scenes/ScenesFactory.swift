//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import UIKit

final class ScenesFactory {

    static let shared = ScenesFactory()

    private init() {}

    func contactsList() -> UIViewController {
        let vc = ContactsListVC()
        vc.viewModel = ContactsListVM()
        vc.viewModel.router = ContactsListRouter(vc: vc)
        return vc
    }

    func contactDetail(_ contact: Contact) -> UIViewController {
        let vc = ContactDetailVC()
        vc.viewModel = ContactDetailVM(contact: contact)
        vc.viewModel.router = ContactDetailRouter(vc: vc)
        return vc
    }

}
