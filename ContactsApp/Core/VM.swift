//
//  Created by Dmitry Sochnev.
//  Copyright © 2020 Applicatura. All rights reserved.
//

import Foundation

protocol VM {
    associatedtype Action

    func reduce(_ action: Action)
}

extension VM {
    func fire(action: Action) {
        reduce(action)
    }
}
